import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the SecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-second',
  templateUrl: 'second.html',
})
export class SecondPage {
  homePage = HomePage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SecondPage');
  }

  backToHome(){
  	this.navCtrl.popToRoot();
  }



}
