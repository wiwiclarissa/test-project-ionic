import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstPage } from '../first/first';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  firstPage = FirstPage;
  params = {nama:"wiwi", umur:20};

  constructor(public navCtrl: NavController) {

  }

  buttonClick(){
  	console.log("Button is clicked");
  }


}
